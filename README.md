<!-- TOC -->
* [Answer](#answer)
  * [Test up](#test-up)
  * [Feature ✅❌](#feature-)
    * [Randomize in a range](#randomize-in-a-range)
    * [Randomize in a range with adaptation](#randomize-in-a-range-with-adaptation)
<!-- TOC -->

# Answer

Just a short java project that answer a number to a question.

## Test up

```shell
mvn test-compile org.pitest:pitest-maven:mutationCoverage
```

## Feature ✅❌

### Start Challenge

Draw a series of question to answer, all questions have numeric response.
Request all concurrent registered to answer it as fast as they could.
Trace the response time and the number of questions resolved (real time).

Two ways answer the next question:
* First to solve launch the next question for all
* All concurrent needs to answer the question to launch the next question.

### Randomize in a range

Return a number between a range, without checking if the response is correct, just guess it.

|Language|Status|
|----|----|
|go|❌|
|next|❌|
|java|❌|
|python|❌|

### Randomize in a range with adaptation

Prerequisite: check service for answer.

Return a number between a range, and check if it is less or more than the answer.

|Language|Status|
|----|----|
|go|❌|
|next|❌|
|java|❌|
|python|❌|

### Cheater time

Prerequisite: Service to retrieve the response for an identifier.

Get the response then  answer

|Language|Status|
|----|----|
|go|❌|
|next|❌|
|java|❌|
|python|❌|

### Lucky Man

Prerequisite: check service for answer.
Return a number between a range, and check if it is the answer. If not retry.

|Language|Status|
|----|----|
|go|❌|
|next|❌|
|java|❌|
|python|❌|

