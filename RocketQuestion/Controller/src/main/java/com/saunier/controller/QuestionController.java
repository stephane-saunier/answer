package com.saunier.controller;

import com.saunier.dto.Question;
import com.saunier.service.QuestionService;
import jakarta.websocket.server.PathParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController()
@RequestMapping(path = "/api/v1")
public class QuestionController {

    private final Logger LOGGER = LoggerFactory.getLogger(QuestionController.class);

    private final QuestionService questionService;

    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping("/question")
    public Page<Question> list(Pageable pageable) {
        final var questions = questionService.list().toList();
        return new PageImpl<>(questions, pageable==null?PageRequest.ofSize(12):pageable, questions.size());
    }

    @GetMapping("/question/{id}")
    public Optional<Question> get(@PathParam("id") String questionId) {
        LOGGER.debug(">> get(): questionId: [{}]", questionId);
        return Optional.of(new Question("1", "What is the response of everything life etc?", 42));
    }
}
