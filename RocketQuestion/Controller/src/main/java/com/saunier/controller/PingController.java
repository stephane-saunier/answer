package com.saunier.controller;

import com.saunier.dto.Health;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
public class PingController {
    public PingController() {
    }

    @GetMapping("/ping")
    public Health ping() {
        return new Health("UP");
    }

}
