package com.saunier.controller;

import com.saunier.controller.utils.TagsForTest;
import com.saunier.dto.Health;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;


public class PingTest {

    @Test()
    @DisplayName("should return health OK")
    @Tag(TagsForTest.MONITORING)
    public void pingMakeMeFamous() {

        var pingController = new PingController();

        Health ping = pingController.ping();
        Assertions.assertEquals("UP", ping.health());

    }


}
