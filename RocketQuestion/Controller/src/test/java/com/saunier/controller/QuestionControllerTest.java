package com.saunier.controller;

import com.saunier.dto.Question;
import com.saunier.provider.QuestionProvider;
import com.saunier.service.QuestionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.stream.Stream;

class QuestionControllerTest {

    @Test
    @DisplayName("should return a list of question, with their id")
    void list() {
        var provider = new QuestionProvider() {
            @Override
            public Stream<Question> list() {
                return Stream.of(new Question("id", "What is the response of everything life etc?", 42));
            }

            @Override
            public Optional<Question> get(String questionId) {
                return Optional.empty();
            }
        };
        var questionService = new QuestionService(provider);

        var questionController = new QuestionController(questionService);

        Assertions.assertEquals("id", questionController.list(Pageable.ofSize(20)).toList().get(0).id());
    }

    @Test
    @DisplayName("should return a unique question with their response, by using their id")
    void get() {
        var provider = new QuestionProvider() {
            @Override
            public Stream<Question> list() {
                return Stream.of();
            }

            @Override
            public Optional<Question> get(String questionId) {
                return Optional.of(new Question("id", "What is the response of everything life etc?", 42));
            }
        };
        var questionService = new QuestionService(provider);

        var questionController = new QuestionController(questionService);

        final var questionOptional = questionController.get("1");
        Assertions.assertTrue(questionOptional.isPresent());
        var question = questionOptional.get();
        Assertions.assertEquals("1", question.id());
        Assertions.assertEquals("What is the response of everything life etc?", question.question());
        Assertions.assertEquals(42, question.response());
    }

    @Test
    @DisplayName("It should return the first element if no pages")
    void listWithoutPageable() {
        var provider = new QuestionProvider() {
            @Override
            public Stream<Question> list() {
                return Stream.of(new Question("id", "What is the response of everything life etc?", 42));
            }

            @Override
            public Optional<Question> get(String questionId) {
                return Optional.empty();
            }
        };
        var questionService = new QuestionService(provider);

        var questionController = new QuestionController(questionService);

        Assertions.assertEquals(12, questionController.list(null).getPageable().getPageSize());
    }
}