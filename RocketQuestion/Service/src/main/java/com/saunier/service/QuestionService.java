package com.saunier.service;

import com.saunier.dto.Question;
import com.saunier.provider.QuestionProvider;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.Stream;

@Service
public class QuestionService {
    private final QuestionProvider questionProvider;

    public QuestionService(QuestionProvider questionProvider) {
        this.questionProvider = questionProvider;
    }

    public Stream<Question> list() {
        return questionProvider.list();
    }
    public Optional<Question> get(String questionId){
        return questionProvider.get(questionId);
    }
}
