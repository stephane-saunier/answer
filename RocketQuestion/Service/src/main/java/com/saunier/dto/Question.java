package com.saunier.dto;

public record Question(String id, String question, Integer response) {
}
