package com.saunier.provider;

import com.saunier.dto.Question;

import java.util.Optional;
import java.util.stream.Stream;

public interface QuestionProvider {
    Stream<Question> list() ;

    Optional<Question> get(String questionId);
}
