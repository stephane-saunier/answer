package com.saunier.service;

import com.saunier.dto.Question;
import com.saunier.provider.QuestionProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.stream.Stream;

class QuestionServiceTest {

    /**
     * { "questions": [{"id": 1}
     */
    @Test
    public void should_list_all_questions() {
        final var questionProvider = new QuestionProvider() {
            @Override
            public Stream<Question> list() {
                return Stream.of(new Question("a2b1f5d9-4cac-436a-88da-a1fcec733c6f", null, null), new Question("a411edfd-0ac6-4952-8b37-85b88b41df10", null, null));
            }

            @Override
            public Optional<Question> get(String questionId) {
                return Optional.empty();
            }
        };
        final var questionService = new QuestionService(questionProvider);

        final var questions = questionService.list().toList();

        Assertions.assertEquals(2, questions.size());
        Assertions.assertEquals("a2b1f5d9-4cac-436a-88da-a1fcec733c6f", questions.get(0).id());
        Assertions.assertEquals("a411edfd-0ac6-4952-8b37-85b88b41df10", questions.get(1).id());
    }

    @Test
    public void should_get_a_question_by_identifier() {
        final var questionService = new QuestionService(new QuestionProvider() {
            @Override
            public Stream<Question> list() {
                return null;
            }

            @Override
            public Optional<Question> get(String questionId) {
                return Optional.of(new Question("1","What is the response of everything life etc?",42));
            }
        });
        Assertions.assertTrue(questionService.get("1").isPresent());
        final var question = questionService.get("1").get();
        Assertions.assertEquals("1", question.id());
        Assertions.assertEquals("What is the response of everything life etc?", question.question());
        Assertions.assertEquals(42, question.response());
    }

    @Test
    public void should_have_a_winner() {

    }

    @Test
    public void should_have_a_list_of_concurrent() {

    }

    @Test
    public void should_have_an_answer() {

    }
}