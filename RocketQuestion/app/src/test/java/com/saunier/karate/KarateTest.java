package com.saunier.karate;

import com.intuit.karate.junit5.Karate;
import com.saunier.application.Application;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT, classes = {Application.class})

public class KarateTest {

    @Karate.Test
    Karate allTest() throws IOException {
        final var file = new File("src/test/karate/feature");
        final var list = file.list();
        final String[] pathToAllFeatures = new String[list.length];
        var index = 0;
        for (String path : list) {
            pathToAllFeatures[index++] = "classpath:./feature/".concat(path);
        }
        return Karate.run(pathToAllFeatures);
    }

}
