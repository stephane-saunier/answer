Feature: Ping

  Background:
    * url baseUrl
    * def pingUrl = '/ping'

  Scenario: should return a status up

    Given path pingUrl
    When method GET
    Then status 200
    And match response == { health:  'UP'}