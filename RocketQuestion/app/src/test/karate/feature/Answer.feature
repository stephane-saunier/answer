Feature: Ping

  Background:
    * url baseUrl
    * def questions = '/api/v1/question'

  Scenario: should get a question by it s id

    Given path questions + "/1"
    When method Get
    Then status 200
    And match response == {"id": "1", question:  "What is the response of everything life etc?", response: 42 }

  Scenario: should list all questions

    Given path questions
    When method Get
    Then status 200
    And match response == {  "content": [  {  "id": "1"  }  ],  "pageable": {  "sort": {  "empty": true,  "sorted": false,  "unsorted": true  },  "offset": 0,  "pageNumber": 0,  "pageSize": 20,  "paged": true,  "unpaged": false  },  "last": true,  "totalPages": 1,  "totalElements": 1,  "size": 20,  "number": 0,  "sort": {  "empty": true,  "sorted": false,  "unsorted": true  },  "first": true,  "numberOfElements": 1,  "empty": false  }
