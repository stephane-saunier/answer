package com.saunier.provider;

import com.saunier.dto.Question;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.stream.Stream;

@Component
public class QuestionBasicProvider implements QuestionProvider {

    @Override
    public Stream<Question> list() {
        return Stream.of(new Question("1", null, null));
    }

    @Override
    public Optional<Question> get(String questionId) {
        return Optional.of(new Question("1","What is the response of everything life etc?",42));
    }
}
