package com.saunier.provider;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class QuestionBasicProviderTest {

    @Test
    public void should_return_a_list_of_one_question(){
        final var provider = new QuestionBasicProvider();
        Assertions.assertEquals("1",provider.list().toList().get(0).id());
    }

    @Test
    public void should_return_a_question(){
        final var provider = new QuestionBasicProvider();


        final var questionOptional = provider.get("1");
        Assertions.assertTrue(questionOptional.isPresent());
        final var question = questionOptional.get();
        Assertions.assertEquals("1", question.id());
        Assertions.assertEquals("What is the response of everything life etc?", question.question());
        Assertions.assertEquals(42, question.response());
    }
}
